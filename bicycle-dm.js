import { LitElement } from 'lit-element';
import '@cells-components/cells-generic-dp';

/**
This component es el DM utilizado para la página del mapa principal.

Example:

```html
<bicycle-dm></bicycle-dm>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class BicycleDm extends LitElement {
  static get is() {
    return 'bicycle-dm';
  }

  // Declare properties
  static get properties() {
    return {
      host: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = "http://localhost:1122";
  }

  /**
   * Obtiene la lista de Staciones.
   */
  loadList() {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');
    console.log('Entra Cuidado')
    let dp = new DP();
    dp.host = this.host;
    dp.path = "bicycle/stationInformation"
    dp.method = "GET"
    dp.timeout = "10000"
    dp.generateRequest()
      .then((res) => this._listSuccess(res))
      .catch(this._createFinally.bind(this));
  }

  /**
   * 
   * @param {String} eventName nombre del evento
   * @param {*} detail Detalle a  enviar.
   * Es un método de utilidad. 
   */
  _emmit(eventName, detail) {
    this.dispatchEvent(new CustomEvent(eventName, { detail, composed: true, bubbles: true }));
  }

  /**
   * Callback ok. Informa con eventos.
   * @param {Object} res Datos de las estaciones
   */
  _listSuccess(res) {
    console.log(res)
    this._emmit('list_stations_success_data', res.response.detail);
    this._emmit('service_loaded', 'fade-out');
  }

  /**
   * Callback de error, informa el estado con eventos.
   * @param {Object} res Detalle del error
   */
  _createFinally(res) {
    this._emmit('message_response', res.response.message);
    this._emmit('show_message', true);
    this._emmit('service_loaded', 'fade-out');
  }

  /**
   * Retorna mediante un evento el id de la estación a cargar.
   * Es utilizado para navegación.
   * @param {Number} station ID de la estación
   */
  changePage(station) {
    const detail = { stationId: station.station_id }
    return this.dispatchEvent(new CustomEvent('load_detail_station', { detail, composed: true, bubbles: true }));
  }
}

// Register the element with the browser
customElements.define(BicycleDm.is, BicycleDm);
